## Responsive (Clothing) Design
*Nina Walia*

https://www.webdirections.org/wds/speakers/nina-walia.php

### Wearables for Engineers:
* Why doesn't the women's version have a phone pocket?
* Where does the gesture identification take place? Where does the neural network live?
* What aspects need to "go to the cloud"?
* How does one build app integrations for jacquard?


A machine learning algorithm to measure and predict success of social innovation ventures.


## Intuitive APIs and Developer Education
*Bear Douglas*

https://www.webdirections.org/wds/speakers/bear-douglas.php

## Derp Driven Development

REST 1999
eBay exposed APIs
Flickr was user generated content
* Provided API SDKs
* Participated in Oauth
* 

Fast forward 2010: Facebook Graph API
REST not super great at representing connections
Open Graph: What exists and how they're connected
Facebook's "Hack the graph" onboarding process sucked

Enterprise OAUTH authorisation is hard
Make APIs easier to use


## GraphQL and The Web: Thinking beyond the technology
*Ben Teese*

https://www.webdirections.org/wds/speakers/ben-teese.php

Started out by talking smack about front-end developers
Then talked smack about back-end developers

Front-end code does too much
    Wanna get more data?
    Call another endpoint
    Get more data from endpoint

BFF Pattern: Back-end for front-end
UI devs build them 

Polymorphism in GraphQL SCHEMA?
Can we bury bodies in Apollo middle-ware?


## SOLID JavaScript
*Steve Morris*

https://www.webdirections.org/wds/speakers/steve-morris.php

Strongly-typed class-based inheritance


objects have state and behaviour
### SOLID

#### Single Responsilibiltiy
* Object should haver just one reason to change
* Tightly encapsulated
* Smaller
* Composition vs Inheritance

* One type per file

#### Open Closed
Open for extension but closed for modification
extensibility without modification

#### Liskov Substitution
* Small classes
* Composition over
* Objects are replacable with all their subtypes

#### Interface Segregation
Many-client specific interfaces are better than one general-purpose interface
Small interfaces over one large one
* leftpad.js LOL

#### Dependency Inversion
Not dependency injection
Depend upon abstractions, not concrete examples

Object destructuring, describes the method interface inside the method signature
Guards and assertions
Decorator, linters, or libraries?

### Bringing it all together
Batman tool

Like database normalisation but for object higherarchy


https://docs.microsoft.com/en-us/azure/architecture/patterns/backends-for-frontends

## Fun with Sensors and Browser APIs for the web!

*Mandy Kerr*

https://www.webdirections.org/wds/speakers/mandy-kerr.php

### Experimental sensor APIs in browser

Remember this is all experimental

Web Speech APIs
speech recognition: understands human voice
speech synthesis: reads text and produces voice


Speech recognition: needs wifi
  interim results: updates past words based on confidence (more real-time)

APIs start() - stop()
onresult()
onspeechend()

SIDE NOTE: Check out variable fonts https://en.wikipedia.org/wiki/Variable_fonts

Accelerometer
DeviceOrientation
Alpha
Beta
Gamma
IDEA: Could use this on Alphero Site for mobile
subtle ui effects

Ambient light sensor
can be in dark mode automatically, when the user visits at night

BlueGX?
BloomGX?

## Machine learning in the browser
@ryanseddon

https://www.webdirections.org/wds/speakers/ryan-seddon.php

Covers:
High Level Machine Learning
TensorFlow.js

Don't need a phd in math to get started

Recognize Cat
BodyPix
StyleTransfer

Why do ML client-side?
knowledge transfer
What is transfer learning?
    Transfer learning is a research problem in machine learning that focuses on storing knowledge gained while solving one problem and applying it to a different but related problem.[1] For example, knowledge gained while learning to recognize cars could apply when trying to recognize trucks. This area of research bears some relation to the long history of psychological literature on transfer of learning, although formal ties between the two fields are limited.

Decentralised projects? Naw

DeepLearning.js
There are pre-trained models

AutoML = Infrastructure Training

Models are pretty big
PWA can cache them offline
Decide when to download them

Further Reading
Teachable Machine
Creatability (Accessibiltiy: visualising sound)

Beyond TensorFlow:
Open Neural Network Exchange ONNX
Everyone in ML but Google: ONNX.js
Runs in web worker (in the background)

WebNN:
Web Nueral Network API
Intel proposal to bring ML inference natively to the browser
Remove the hack of WebGL
Custom build of chromium that is more performant
They have a polyfill available today

More resources
github.com/webmachinelearning/webnn
ml5.js
observable
obervablehq.com


## My voice is my passport. Verify me?

*Ben Dechrai*

https://www.webdirections.org/wds/speakers/ben-dechrai.php

    Security is hard. Using the same password for everything is easy.

Biometrics

Talking about auth

##  Strike a pose - Gesture recognition in JavaScript with Machine Learning & Arduino.

*Charlie Gerard*

https://www.webdirections.org/wds/speakers/charlie-gerard.php

    Most of our interactions with technology aren’t really intuitive. We’ve had to adapt to it by learning to type, swipe, execute specific voice commands, etc… but what if we could train technology to adapt to us?

