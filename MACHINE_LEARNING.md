Statistics

R, Python, SQL

Excellent understanding of machine learning techniques such as regression, classifiers and clustering methods

Familiar with deep learning methods to process high-dimensional and unstructured data and related tool kits such as Tensorflow

Good applied statistics skills, such as hypothesis testing and understanding of probability distributions

Understanding of NoSQL databases and distributed computing platforms e.g. Apache Spark

Proven working experience in technical writing of software documentation.

A high level of interpersonal skills and ability to collaborate

