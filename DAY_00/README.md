# Day 0: Culture

## Extreme Cooperation of Superorganisms: Four Lessons Humans Can Learn from Honey Bees

*Peter Moskovits* (Gave out gifts to increase engagement)

https://www.webdirections.org/culture/speakers/peter-moskovits.php


### Honeybee Democracy

Teams as superorganisms

What is needed for cooperation? Communication
Protocols
Waggle Dance
Bees as a group act like a larger organism: Moving, replicating,

A machine learning algorithm to measure and predict success of social innovation ventures.


## A rational way of understanding and scaling Culture

*Mahesh Muralidhar*

https://www.webdirections.org/culture/speakers/mahesh-muralidhar.php

Worked in NZ
Measuring and culture in order to scale it

Controversial viewpoint: we all have bias


## Designing inclusion into distributed teams

*Patima Tantiprasut*

https://www.webdirections.org/culture/speakers/patima-tantiprasut.php

1. Instead of striving for 100% inclusion strive for _the absence of exclusion_
1. Foster sense of belonging
1. Develop a culture playbook

    There's no denying that flexibility is fast becoming a requirement when forming modern teams. We're seeing some great example patterns for successful remote, onsite and distributed team structures when it comes to scaling delivery, but how do we bring people closer together when they're getting further apart? Let's talk about scaling culture and deliberately designing in inclusive habits for the whole team.

IceBreakER?
One person per scream?


## Gender Balance

*Emma Jones*

https://www.webdirections.org/culture/speakers/emma-jones.php

Achieving gender balance in tech is still frustratingly challenging. CEOs are more aware than ever that diverse teams are a primary enabler of effective problem solving, innovation and profitability. While HR helps by fostering diversity in general, technology has proven a unique subculture that traditional D&I initiatives simply don't work for.


Lack of Support from Managers
Lack of Career Progression
Poor Culture

1966 William Cannon

Men Championing Change

## The power of saying "I don't know

*Andrew Murphy*

https://www.webdirections.org/culture/speakers/andrew-murphy.php

    It's something we all struggle with, admitting we don't know something. But I'm here to show you the power of saying “I don't know” to people.


Ego
Imposter Syndrome
Don't need to be smartest
Don't need to know every answer

Answers:
  I don't know but, ...
  I don't know, do you?

How can we augment annual PDRs?

Conflict & Feedback


## Scaling and trying not to break things...too much

*Jen Mumford*

https://www.webdirections.org/culture/speakers/jen-mumford.php


Feedback is important 
Triangle Conversations are bad
    Respond with "What did they say when you brought this up with them?"

Develop a compensation philosophy

"9 Box Talent Matrix"?

Listen more


## Three experiments for culture leaders

*Eugene Chung*

https://www.webdirections.org/culture/speakers/eugene-chung.php

    Why is it so challenging for People and Culture leaders to create meaningful change within their organisations?

Ask team members: What's holding you back from doing your best work?

What's the learning loop?


## Workstyles: the 4 basic styles & why leaders must be adaptive

*Kristi Riordan*

https://www.webdirections.org/culture/speakers/kristi-riordan.php

    Ever wondered why working with some people feels a bit like rowing with the current when others can feel like rowing upstream? Had communication that crossed like two ships passing in the night? Kristi is passionate about culture, communication and helping people reach their fullest potential. Having spent the past 15 years leading NYC tech companies, she's excited to share some frameworks to help you spot individual workstyles, develop adaptability and create a more productive work environment for everyone.

Deputize your team
    Working groups

Culture is a victim of the tragedy of commons
Everybody owns it and nobody cares about it


## Mastering Collaboration

*Gretchen Anderson*

https://www.webdirections.org/culture/speakers/gretchen-anderson.php

Tell a story (aka "Previously on Lost")

Help the world understand what the world will be like after we succeed.

Co-operation vs collaboration vs co-creation

Leaders connect bubbles

## Counter Culture

Rob Pyne

Good leaders can disagree and commit
Talked about internal protocols at Amazon & Google

Create and foster debate in a safe environment
Red Team / Blue Team
"Mining for conflict"
Meeting Arcs that encourage divergent thinking and then switch over to convergent thinking

Ella Bache CEO 
1-3-1 Rule

rob@realizer.com.au







